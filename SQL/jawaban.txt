1. Buat Database
 create database myshop;

2. Membuat Table di Dalam Database
    Table Users
    create table users(
        -> id int(8) primary key auto_increment,
        -> name varchar(255),
        -> email varchar(255),
        -> password varchar(255)
        -> );

    +----------+--------------+------+-----+---------+----------------+
    | Field    | Type         | Null | Key | Default | Extra          |
    +----------+--------------+------+-----+---------+----------------+
    | id       | int(8)       | NO   | PRI | NULL    | auto_increment |
    | name     | varchar(255) | YES  |     | NULL    |                |
    | email    | varchar(255) | YES  |     | NULL    |                |
    | password | varchar(255) | YES  |     | NULL    |                |
    +----------+--------------+------+-----+---------+----------------+

    Table Categories
    create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );
    +-------+--------------+------+-----+---------+----------------+
    | Field | Type         | Null | Key | Default | Extra          |
    +-------+--------------+------+-----+---------+----------------+
    | id    | int(8)       | NO   | PRI | NULL    | auto_increment |
    | name  | varchar(255) | YES  |     | NULL    |                |
    +-------+--------------+------+-----+---------+----------------+

    Table Items
    create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> prince int(25),
    -> stock int(10),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );
    +-------------+--------------+------+-----+---------+----------------+
    | Field       | Type         | Null | Key | Default | Extra          |
    +-------------+--------------+------+-----+---------+----------------+
    | id          | int(8)       | NO   | PRI | NULL    | auto_increment |
    | name        | varchar(255) | YES  |     | NULL    |                |
    | description | varchar(255) | YES  |     | NULL    |                |
    | prince      | int(25)      | YES  |     | NULL    |                |
    | stock       | int(10)      | YES  |     | NULL    |                |
    | category_id | int(8)       | YES  | MUL | NULL    |                |
    +-------------+--------------+------+-----+---------+----------------+

3. Memasukkan Data pada Table
    Table Users
    insert into users(name, email, password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
    +----+----------+--------------+-----------+
    | id | name     | email        | password  |
    +----+----------+--------------+-----------+
    |  1 | John Doe | john@doe.com | john123   |
    |  2 | Jane Doe | jane@doe.com | jenita123 |
    +----+----------+--------------+-----------+
    
    Table Categories
    insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
    +----+---------+
    | id | name    |
    +----+---------+
    |  1 | gadget  |
    |  2 | cloth   |
    |  3 | men     |
    |  4 | women   |
    |  5 | branded |
    +----+---------+

    Table Items
    insert into items(name, description, prince, stock, category_id) values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);
    +----+-------------+-----------------------------------+---------+-------+-------------+
    | id | name        | description                       | prince  | stock | category_id |
    +----+-------------+-----------------------------------+---------+-------+-------------+
    |  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
    |  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
    |  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
    +----+-------------+-----------------------------------+---------+-------+-------------+

4. Mengambil Data dari Database
    a. Mengambil data users (kecuali password)
        select id,name,email from users;
        +----+----------+--------------+
        | id | name     | email        |
        +----+----------+--------------+
        |  1 | John Doe | john@doe.com |
        |  2 | Jane Doe | jane@doe.com |
        +----+----------+--------------+

    b. Mengambil data items (price>1000000)
        select * from items where prince>1000000;
        +----+-------------+-----------------------------------+---------+-------+-------------+
        | id | name        | description                       | prince  | stock | category_id |
        +----+-------------+-----------------------------------+---------+-------+-------------+
        |  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
        |  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
        +----+-------------+-----------------------------------+---------+-------+-------------+

        select * from items where name like '%watch';
        +----+------------+-----------------------------------+---------+-------+-------------+
        | id | name       | description                       | prince  | stock | category_id |
        +----+------------+-----------------------------------+---------+-------+-------------+
        |  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
        +----+------------+-----------------------------------+---------+-------+-------------+

    c. Menampilkan data items join dengan kategori
        select items.name, items.description, items.prince, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
        +-------------+-----------------------------------+---------+-------+-------------+--------+
        | name        | description                       | prince  | stock | category_id | name   |
        +-------------+-----------------------------------+---------+-------+-------------+--------+
        | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
        | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
        | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
        +-------------+-----------------------------------+---------+-------+-------------+--------+

5. Mengubah Data dari Database
    update items set prince=2500000 where id=1;
    +----+-------------+-----------------------------------+---------+-------+-------------+
    | id | name        | description                       | prince  | stock | category_id |
    +----+-------------+-----------------------------------+---------+-------+-------------+
    |  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
    |  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
    |  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
    +----+-------------+-----------------------------------+---------+-------+-------------+
